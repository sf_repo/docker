#!/bin/bash

if [[ ! -d /opt/favicon/out ]]
then
    mkdir /opt/favicon/out
fi

wget https://${1}/favicon -o out/${1}.ico
