FROM centos:7 as b
RUN yum install -y wget
COPY fav.sh hosts.txt /opt/favicon/

FROM scratch
COPY --from=b /lib64/libssl.so.10 /lib64/libcrypto.so.10 /lib64/libz.so.1 /lib64/libdl.so.2 /lib64/libidn.so.11 /lib64/libuuid.so.1 /lib64/libpcre.so.1 /lib64/libc.so.6 /lib64/libgssapi_krb5.so.2 /lib64/libkrb5.so.3 /lib64/libcom_err.so.2 /lib64/libk5crypto.so.3 /lib64/libpthread.so.0 /lib64/libkrb5support.so.0 /lib64/libkeyutils.so.1 /lib64/libresolv.so.2 /lib64/libselinux.so.1 /lib64/libtinfo.so.5 /lib64/libdl.so.2 /lib64/libc.so.6 /lib64/ld-linux-x86-64.so.2 /lib64/
COPY --from=b /bin/sh /bin/bash /bin/
COPY --from=b /usr/bin/sh /usr/bin/bash /usr/bin/wget /usr/bin/cat /usr/bin/
COPY --from=b /opt/favicon/fav.sh /opt/favicon/hosts.txt /opt/favicon/
#COPY fav.sh hosts.txt /opt/favicon/
WORKDIR /opt/favicon
CMD bash /opt/favicon/fav.sh `cat /opt/favicon/hosts.txt`
